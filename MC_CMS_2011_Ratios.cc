// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/BinnedHistogram.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


   /// CMS ratio of 3-jet to 2-jet cross-sections
   class MC_CMS_2011_Ratios : public Analysis {
   public:


     MC_CMS_2011_Ratios()
       : Analysis("MC_CMS_2011_Ratios") {  
     }


     void init() {
       FinalState fs;
       FastJets akt(fs, FastJets::ANTIKT, 0.5);
       addProjection(akt, "antikT");

       _h_tmp_dijet    = bookHisto1D("jet2",25,100.,2600.);
       _h_tmp_trijet   = bookHisto1D("jet3",25,100.,2600.);
       _h_tmp_quadjet  = bookHisto1D("jet4",25,100.,2600.);
       _h_tmp_quintjet = bookHisto1D("jet5",25,100.,2600.);
       _h_r32 = bookScatter2D("Ratio_32", 25, 100., 2600.);
       _h_r43 = bookScatter2D("Ratio_43", 25, 100., 2600.);
       _h_r54 = bookScatter2D("Ratio_54", 25, 100., 2600.);
     }


     void analyze(const Event & event) {
       const double weight = event.weight();

       Jets highpT_jets;
       double HT = 0;
       foreach(const Jet & jet, 
	       applyProjection<JetAlg>(event, "antikT").jetsByPt(50.0*GeV)) {
         if (fabs(jet.eta()) < 2.5) {
           highpT_jets.push_back(jet);
           HT += jet.pT();
         }
       }
       if (highpT_jets.size() < 2) vetoEvent;
       if (highpT_jets.size() >= 2) _h_tmp_dijet->fill(HT, weight);
       if (highpT_jets.size() >= 3) _h_tmp_trijet->fill(HT, weight);
       if (highpT_jets.size() >= 4) _h_tmp_quadjet->fill(HT, weight);
       if (highpT_jets.size() >= 5) _h_tmp_quintjet->fill(HT, weight);
     }


     void finalize() {
       divide(_h_tmp_trijet,   _h_tmp_dijet,   _h_r32);
       divide(_h_tmp_quadjet,  _h_tmp_trijet,  _h_r43);
       divide(_h_tmp_quintjet, _h_tmp_quadjet, _h_r54);
     }


   private:

     Histo1DPtr _h_tmp_dijet, _h_tmp_trijet, _h_tmp_quadjet, _h_tmp_quintjet;
     Scatter2DPtr _h_r32, _h_r43, _h_r54;

  };


  // A hook for the plugin system
  DECLARE_RIVET_PLUGIN(MC_CMS_2011_Ratios);

}
