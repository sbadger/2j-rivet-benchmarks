# BEGIN PLOT /MC_ptH/.*
RatioPlotSameStyle=1
# END PLOT

# BEGIN PLOT /MC_CMS_2011_Ratios/Ratio_32
Title=$R_{32}(H_T)$ -- 3- to 2-jet ratio in dependence on $H_T$
XLabel=$H_T$   [GeV]
YLabel=$R_{32}$   [1/GeV]
YMax=1.2
YMin=0.0
LogY=0
RatioPlotSameStyle=1
LegendXPos=0.75
LegendYPos=0.25
# END PLOT

# BEGIN PLOT /MC_CMS_2011_Ratios/Ratio_43
Title=$R_{43}(H_T)$ -- 4- to 3-jet ratio in dependence on $H_T$
XLabel=$H_T$   [GeV]
YLabel=$R_{43}$   [1/GeV]
YMax=1.2
YMin=0.0
LogY=0
RatioPlotSameStyle=1
LegendXPos=0.75
LegendYPos=0.25
# END PLOT

# BEGIN PLOT /MC_CMS_2011_Ratios/Ratio_54
Title=$R_{54}(H_T)$ -- 5- to 4-jet ratio in dependence on $H_T$
XLabel=$H_T$   [GeV]
YLabel=$R_{54}$   [1/GeV]
YMax=1.2
YMin=0.0
LogY=0
RatioPlotSameStyle=1
LegendXPos=0.75
LegendYPos=0.25
# END PLOT

